import logo from './logo.svg'
import './App.css'
import './css/cbutton.css'
import './css/checkbox.css'
import './css/inputbox.css'
import './css/pop.css'
import React from 'react'
import AddLine from './components/AddLine'
import PopComponent from './components/PopComponent'
import TodoLine from './components/TodoLine'
import Menu from './components/Menu'
import { Mode, Language, AlertMode } from './constants'
import { withTranslation } from 'react-i18next'
import i18n from './i18n'

class App extends React.Component{
  constructor(props){
    super(props)

    console.log('local storage', localStorage.getItem('toDoLineData'))
    const data = localStorage.getItem('toDoLineData') 
    ? JSON.parse(localStorage.getItem('toDoLineData')) 
    : [{
      key: Date.now(),
      displayName: 'Default',
      memos: [],
      idx: 0
    }]
    
    const storeLanguage = localStorage.getItem('language')
    ? localStorage.getItem('language')
    : Language.EN
    i18n.changeLanguage(storeLanguage)
    console.log('store language', storeLanguage)

    this.state = {
      groups: data,
      currentGroupIndex: 0,
      filtBy: Mode.None,
      language: storeLanguage,
      pop: false,
      popTitle: 'XXXXX',
      popContent: 'XXXXX',
      alert: false,
      alertType: AlertMode.SUCCESS,
      alertContent: '',
      timerId: null
    }

    this.resolve = null
    this.reject = null
  }

  getCurrentGroupMemos = () => {
    return this.state.groups[this.state.currentGroupIndex].memos
  }

  updateGroup = (memos, caller = 'unset') => {
    const copy = this.state.groups.slice()
    copy[this.state.currentGroupIndex].memos = memos
    this.setState({
      groups: copy
    })
    console.log(caller, copy)
    localStorage.setItem('toDoLineData', JSON.stringify(copy))
  }

  addGroupAndSwitch = (displayName) => {
    if(displayName === '')
      return
    const obj = {
      key: Date.now(),
      displayName: displayName,
      memos: [],
      idx: this.state.groups.length
    }
    this.setState({
      groups: [...this.state.groups, obj],
      currentGroupIndex: this.state.groups.length,
      filtBy: Mode.None
    })
    localStorage.setItem('toDoLineData', JSON.stringify(
      [...this.state.groups, obj]
    ))
    console.log('switch to idx:', this.state.groups.length)
    this.showAlert(i18n.t('alert-add', { type: i18n.t('success') }))
  }

  switchGroup = (e, idx) => {
    console.log('switch to idx:', idx)
    console.log('groups', this.state.groups)
    this.setState({
      currentGroupIndex: idx,
      filtBy: Mode.None
    })
  }

  deleteGroup = () => {
    const cidx = this.state.currentGroupIndex
    if(cidx !== 0){
      this.switchGroup(undefined, cidx - 1)
    }
    const copy = this.state.groups.slice()
    copy.splice(cidx, 1)
    copy.forEach((group, idx) => group.idx = idx)
    this.setState({
      groups: copy.map((group, idx) => {
        group.idx = idx
        return group
      })
    })
    localStorage.setItem('toDoLineData', JSON.stringify(
      copy.map((group, idx) => {
        group.idx = idx
        return group
      })
    ))
    this.showAlert(i18n.t('alert-delete', { type: i18n.t('success') }))
  }

  loadGroups = (loadedGroups) => {
    console.log('load group', loadedGroups)
    this.setState({
      groups: loadedGroups,
      currentGroupIndex: 0,
      filtBy: Mode.None
    })
    localStorage.setItem('toDoLineData', JSON.stringify(loadedGroups))
    this.showAlert(i18n.t('alert-load', { type: i18n.t('success') }), 1500)
  }

  addMemo = (memo) => {
    const obj = {
      key: Date.now(),
      content: memo,
      finish: false,
    }
    const newMemos = [...this.getCurrentGroupMemos(), obj]
    this.updateGroup(newMemos, 'add')
    this.showAlert(i18n.t('alert-add', { type: i18n.t('success') }))
  }

  finishMemo = (key, finish) => {
    const copy = this.getCurrentGroupMemos().slice()
    const idx = copy.findIndex(memo => memo.key === key)
    copy[idx].finish = finish
    copy[idx].key = Date.now()
    this.updateGroup(copy, 'finish')
  }

  deleteMemo = (key) => {
    const copy = this.getCurrentGroupMemos().slice()
    const idx = copy.findIndex(memo => memo.key === key)
    copy.splice(idx, 1)
    this.updateGroup(copy, 'delete')
    this.showAlert(i18n.t('alert-delete', { type: i18n.t('success') }))
  }

  editMemo = (key, content) => {
    const copy = this.getCurrentGroupMemos().slice()
    const idx = copy.findIndex(memo => memo.key === key)
    copy[idx].content = content
    copy[idx].key = Date.now()
    this.updateGroup(copy, 'edit')
    this.showAlert(i18n.t('alert-edit', { type: i18n.t('success') }))
  }

  changeFilter = () => {
    switch(this.state.filtBy){
      case Mode.None:
        this.setState({ filtBy: Mode.Todo })
        break;
      case Mode.Todo:
        this.setState({ filtBy: Mode.Done })
        break;
      case Mode.Done:
        this.setState({ filtBy: Mode.None })
        break;
      default:
        console.log('FilterError', this.state.filtBy)
        break;
    }
  }

  switchLanguage = () => {
    switch(this.state.language){
      case Language.EN:
        this.setState({ language: Language.TW })
        i18n.changeLanguage(Language.TW)
        localStorage.setItem('language', Language.TW)
        break;
      case Language.TW:
        this.setState({ language: Language.EN })
        i18n.changeLanguage(Language.EN)
        localStorage.setItem('language', Language.EN)
        break;
      default:
        console.log('LanguageError', this.state.language)
        break;
    }
    console.log('language save to', localStorage.getItem('language'))
  }

  popOut = (resolve, reject, content='XXXXX', title = i18n.t('pop-title-default')) => {
    this.resolve = resolve
    this.reject = reject
    this.setState({ 
      pop: true,
      popTitle: title,
      popContent: content,
    })
  }

  popOff = () => {
    this.setState({ 
      pop: false,
      popTitle: '',
      popContent: '',
    })
  }

  showAlert = (content='XXXX', duration=1000, type=AlertMode.SUCCESS) => {
    this.setState({
      alert: true,
      alertType: type,
      alertContent: content
    })

    const tid = setTimeout(() => {
      this.setState({
        alert: false,
        alertType: AlertMode.SUCCESS,
        alertContent: '',
        timerId: null
      })
    }, duration)
    if(this.state.timerId !== null){
      clearTimeout(this.state.timerId)
    }
    this.setState({ timerId: tid })
  }

  render(){
    const { t } = this.props
    return (
      <div className="App">
        <header className="App-header">
          <Menu 
            currentGroupIndex={this.state.currentGroupIndex}
            groups={this.state.groups}
            changeFilter={this.changeFilter}
            filtBy={this.state.filtBy}
            switchLanguage={this.switchLanguage}
            addGroup={this.addGroupAndSwitch}
            switchGroup={this.switchGroup}
            deleteGroup={this.deleteGroup}
            loadGroups={this.loadGroups}
            pop={this.popOut}
          />

          <PopComponent 
            pop={this.state.pop}
            title={this.state.popTitle}
            content={this.state.popContent}
            popOff={this.popOff}
            resolve={this.resolve}
            reject={this.reject}
          />

          <img src={logo} className="App-logo" alt="logo" />

          {
            this.state.alert
            ? <h3 className={this.state.alertType}>
                {this.state.alertContent}
              </h3>
            : <h3 id="title">{t('title')}</h3>
          }

          <div id="addline">
            <AddLine addEvent={this.addMemo}/>
          </div>

          <div className="todo-container">
            {
              this.getCurrentGroupMemos()
              .filter(memo => {
                switch (this.state.filtBy) {
                  case Mode.Done:
                    return memo.finish === true
                  case Mode.Todo:
                    return memo.finish === false
                  default:
                    return true
                }
              })
              .map((memo, idx) => {
                return (
                  <TodoLine 
                    key={memo.key} 
                    memo={memo} 
                    idx={idx} 
                    finishMemo={this.finishMemo}
                    deleteMemo={this.deleteMemo}
                    editMemo={this.editMemo}
                  />
                )
              })
            }
          </div>
        </header>
      </div>
    )
  }
}

export default withTranslation()(App);