import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import en from './locales/en/translation.json'
import tw from './locales/zh-TW/translation.json'

const resources = {
    'en': {
        translation: en,
    },
    'zh-TW': {
        translation: tw,
    },
}

i18n.use(initReactI18next).init({
    resources,
    lng: 'en',
    fallbackLng: 'en',
    interpolation: {
        escapeValue: false
    }
})

export default i18n