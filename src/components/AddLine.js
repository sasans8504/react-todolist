import React from 'react'
import i18n from '../i18n'

export default class AddLine extends React.Component{
    state = {
      content: ''
    }
  
    addMemo = () => {
      if(this.state.content === '')
        return
      this.props.addEvent(this.state.content)
      this.setState({
        content: ''
      })
    }

    changeMemo = (e) => {
      this.setState({content: e.target.value})
    }

    enter2Save = (e) => {
      if(e.key === 'Enter'){
        this.addMemo()
      }
    }
  
    render(){
      return(
        <div className="add-container">
          <input 
            type="text" 
            className="text"
            onChange={this.changeMemo} 
            onKeyPress={this.enter2Save}
            value={this.state.content}
            placeholder={i18n.t('add-line-placeholder')}
          />
          
          <button 
            className="cbutton" 
            onClick={this.addMemo}>
            {i18n.t('button-add')}
          </button>
        </div>
      )
    }
  }