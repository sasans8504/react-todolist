import React from 'react'

export default class EditBox extends React.Component{
  state = {
    width: 0,
  }

  componentDidMount() {
    requestAnimationFrame(() => {
      this.setState({ width: "95%" })
    })
  }

  changeContent = (e) => {
    this.props.contentChange(e.target.value)
  }

  enter2Save = (e) => {
    if(e.key === 'Enter'){
      this.props.saveHandle()
    }
  }

  render() {
    return (
      <>
        <input 
          type="text" 
          style={{ 
            width: this.state.width,
            textAlign: this.props.textAlign
          }} 
          className="text edit" 
          defaultValue={this.props.content}
          onChange={this.changeContent}
          onKeyPress={this.enter2Save}
        />
      </>
    )
  }
}

EditBox.defaultProps = {
  textAlign: 'left',
  contentChange: (...any) => {},
  saveHandle: (...any) => {}
}