import React from 'react'
import '../css/todoline.css'
import EditComponent from './EditComponent'
import i18n from '../i18n'

export default class TodoLine extends React.Component{
  saveMemo = (content) => {
    if(content === '')
      return
    this.props.editMemo(this.props.memo.key, content)
  }

  finishMemo = (e) => {
    this.props.finishMemo(this.props.memo.key, e.target.checked)
  }

  render(){
    return(
      <div className="line-container">
        {
          this.props.idx < 9
          ? <code>0{this.props.idx+1}.</code>
          : <code>{this.props.idx+1}.</code>
        }
        
        <input 
          className="checkbox" 
          type="checkbox" 
          onChange={this.finishMemo}
          checked={this.props.memo.finish}
        />

        <span className="line-content-outside">
          {
            this.props.memo.finish
            ? <label className="line-content marquee"><del>{this.props.memo.content}</del></label>
            : <label className="line-content marquee">{this.props.memo.content}</label>
          }
        </span>

        <EditComponent 
          endEditText={i18n.t('button-save')}
          doEditText={i18n.t('button-edit')}
          defaultEditContent={this.props.memo.content}
          cancelButtonBefore={
            <button 
              className="cbutton" 
              onClick={() => this.props.deleteMemo(this.props.memo.key)}>
              {i18n.t('button-delete')}
            </button>
          }
          endEditAction={this.saveMemo}
        />
      </div>
    )
  }
}