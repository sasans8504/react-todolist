import React from 'react'
import i18n from '../i18n'
import EditBox from './EditBox'

export default class EditComponent extends React.Component {
  state = {
    onEdit: false,
    editContent: ''
  }

  changeEditContent = (content) => {
    this.setState({ editContent: content })
  }

  doEdit = () => {
    this.setState({ onEdit: true })
  }

  endEdit = () => {
    this.setState({ onEdit: false })
  }

  endEditWithAction = () => {
    this.props.endEditAction(this.state.editContent)
    this.endEdit()
  }

  render(){
    return (
      <>
        {
          this.state.onEdit
          ? <button 
              className="cbutton" 
              onClick={this.endEditWithAction}>
              {this.props.endEditText}
            </button>
          : <button 
              className="cbutton" 
              onClick={this.doEdit}>
              {this.props.doEditText}
            </button>
        }

        {
          this.state.onEdit
          ? <button 
              className="cbutton" 
              onClick={this.endEdit}>
              {i18n.t('button-cancel')}
            </button>
          : this.props.cancelButtonBefore
        }

        {
          this.state.onEdit &&
          <EditBox 
            textAlign='center'
            saveHandle={this.endEditWithAction}
            contentChange={this.changeEditContent}
            content={this.props.defaultEditContent}
          />
        }
      </>
    )
  }
}

EditComponent.defaultProps = {
  endEditText: 'Xxxx',
  doEditText: 'Xxxx',
  defaultEditContent: '',
  cancelButtonBefore: <button className="cbutton">xxxx</button>,
  endEditAction: (...any) => {}
}