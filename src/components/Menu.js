import React from 'react'
import EditComponent from './EditComponent'
import '../css/dropbox.css'
import '../css/menu.css'
import i18n from '../i18n'
import Papa from 'papaparse'
import download from 'downloadjs'

export default class Menu extends React.Component{
  constructor(props){
    super(props)
    
    this.state = {
      display: false,
      dropdown: false
    }

    this.hiddenFileInput = React.createRef();
  }

  switchDisplay = () => {
    if(this.state.dropdown){
      this.showDropdown()
    }else{
      this.setState({ display: !this.state.display })
    }
  }

  showDropdown = () => {
    this.setState({ dropdown: !this.state.dropdown })
  }

  addGroup = (name) => {
    this.props.addGroup(name)
  }

  deleteGroup = async () => {
    await new Promise((resolve, reject) => {
      this.props.pop(
        resolve,
        reject,
        i18n.t('delete-notice-content'),
      )
    }).then(() => {
      this.props.deleteGroup()
    }).catch(() => {
      console.log('cancel group delete')
    })
  }

  /*
    \ufeff is byte order mark (BOM) 
    to prevent garbled chinese text in Excel
  */
  exportCSV = () => {
    const csv = Papa.unparse(JSON.stringify(this.props.groups))
    console.log('export', csv)
    download('\ufeff'+csv, "todolist.csv", "text/plain")
  }

  exportJSON = () => {
    const json = JSON.stringify(this.props.groups)
    console.log('export json', json)
    download(json, 'todolist.txt', 'text/plain')
  }

  importCSV = async () => {
    // this.switchDisplay()
    await new Promise((resolve, reject) => {
      this.props.pop(
        resolve,
        reject,
        i18n.t('import-notice-content'),
      )
    }).then(() => {
      // console.log('do import')
      this.hiddenFileInput.current.click()
    }).catch(() => {
      console.log('cancel import')
    })
  }

  importJSON = async () => {
    await new Promise((resolve, reject) => {
      this.props.pop(
        resolve,
        reject,
        i18n.t('import-notice-content'),
      )
    }).then(() => {
      console.log('do import')
      this.hiddenFileInput.current.click()
    }).catch(() => {
      console.log('cancel import')
    })
  }

  fileUploadByCSV = (e) => {
    const fileUploaded = e.target.files[0]
    Papa.parse(fileUploaded, {
      complete: (results) => {
        // this.props.loadGroups(results.data)
        console.log('import', results.data)
      }
    })
  }

  fileUploadByJSON = (e) => {
    const fileUploaded = e.target.files[0]
    const reader = new FileReader()
    reader.onload = ((reader) => {
      return () => {
        console.log('file reader onload')
        this.props.loadGroups(JSON.parse(reader.result))
        e.target.value = null
        console.log('clear value')
      }
    })(reader)
    reader.readAsText(fileUploaded)
  }

  groupContainer = () => {
    return (
      <div className="group-container">
        <input 
          type="file"
          ref={this.hiddenFileInput}
          onChange={this.fileUploadByJSON}
          style={{display:'none'}}
        />
        <div>
          <button 
            className="cbutton"
            onClick={this.exportJSON}>
            {i18n.t('menu-export')}
          </button>
          <button 
            className="cbutton"
            onClick={this.importJSON}>
            {i18n.t('menu-import')}
          </button>

          <button 
            className="cbutton"
            onClick={this.props.switchLanguage}>
            {i18n.t('language')}
          </button>
        </div>

        <div>
          <button 
            className="cbutton" 
            onClick={this.props.changeFilter}>
            {i18n.t('filter')} : {i18n.t('filter-'+this.props.filtBy)}
          </button>
        </div>

        <div>
          <EditComponent 
            endEditText={i18n.t('button-add')}
            doEditText={i18n.t('button-add-group')}
            cancelButtonBefore={
              <button 
                className="cbutton" 
                onClick={this.deleteGroup} 
                disabled={this.props.groups.length === 1}>
                {i18n.t('button-delete-group')}
              </button>
            }
            endEditAction={this.addGroup}
          />

          <div className="dropdown">
            <button className="dropdown-button" onClick={this.showDropdown}>
              {this.props.groups[this.props.currentGroupIndex].displayName}
            </button>
            <div className="dropdown-content">
              {
                this.state.dropdown &&
                this.props.groups
                .filter(group => group.idx !== this.props.currentGroupIndex)
                .map(group => {
                  return (
                    <button 
                      key={Math.random()}
                      onClick={(e) => this.props.switchGroup(e, group.idx)}>
                      {group.displayName}
                    </button>
                  )
                })
              }
            </div>
          </div>
        </div>
      </div>
    )
  }

  render(){
    return (
      <>
        <div className="menu">
          {
            this.state.display &&
            this.groupContainer()
          }
          
          <button 
            className="transparentButton" 
            onClick={this.switchDisplay}>
            {
              this.state.display
              ? i18n.t('menu-close')
              : i18n.t('menu-title')
            }
          </button>
        </div>
        {
          this.state.display &&
          <div className="transparent background" onClick={this.switchDisplay}/>
        }
      </>
    )
  }
}