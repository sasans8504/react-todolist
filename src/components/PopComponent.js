import React from 'react'
import i18n from '../i18n'

export default class PopComponent extends React.Component{
  doConfirm = () => {
    this.props.resolve()
    this.props.popOff()
  }

  doCancel = () => {
    this.props.reject()
    this.props.popOff()
  }

  render(){
    return(
      <>
        {
          this.props.pop &&
          <div className="black background" onClick={this.doCancel}/>
        }
        <div className="popup-root">
          {
            this.props.pop &&
            <div className="popup-container">
              <div className="popup-title">
                {this.props.title}
              </div>

              <div className="popup-content">
                <span>{this.props.content}</span>
              </div>
              
              <button 
                className="cbutton"
                onClick={this.doConfirm}>
                {i18n.t('pop-confirm')}
              </button>
              <button 
                className="cbutton"
                onClick={this.doCancel}>
                {i18n.t('pop-cancel')}
              </button>
            </div>
          }
        </div>
      </>
    )
  }
}