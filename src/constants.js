export const Mode = {
    None: 'none',
    Done: 'done',
    Todo: 'todo'
}

export const Language = {
    EN: 'en',
    TW: 'zh-TW'
}

export const AlertMode = {
    SUCCESS: 'alert-success',
    FAIL: 'alert-fail',
    NOTICE: 'alert-notice'
}